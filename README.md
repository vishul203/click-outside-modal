# Features!

  - Angular 2+ Modal with click outside modal event.
  - Supports multiple modal.

# Sample Code
For Trigger Button
```sh
<div triggerModal [modalTargetId]="ref-Id"> My Button </div>
```

For Modal

```sh
<div #ref-Id clkOutsideModal> Modal Box </div>
```
> IMPORTANT:  Here, ref-id has to be same for both [modalTargetId] of triggering element and id i.e #ref-id of modal to be triggred on modalTargetId click.

### Installation
```sh
npm install --save click-outside-modal
```
----

#Note

For Angualr 6+ Please update path in tsconfig.json as mentioned below
Make sure you will write this code inbetween compileOnSave & compilerOptions.
```sh
"include": [
    "src/**/*.ts",
    "node_modules/click-outside-modal/*"
  ],
```
----

# Usage

Add ClickOutsideModule to your list of module imports:
```sh
import { ClickOutsideModalModule } from 'click-outside-modal/click-oustide-modal.module';
 
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, ClickOutsideModalModule],
  bootstrap: [AppComponent]
})
class AppModule {}
```
----
# License

ISC



