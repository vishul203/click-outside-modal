import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[triggerModal]'
})
// tslint:disable-next-line:class-name
export class triggerModalDirective {
    @Input() modalTargetId: any;
    ref: any;
    constructor(el: ElementRef) {
        this.ref = el.nativeElement;
    }

    @HostListener('click') click() {
       this.modalTargetId.style.display = 'block';
    }
}
