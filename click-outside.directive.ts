import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[clkOutsideModal]'
})
export class ClickOutsideModalDirective {
    ref: any;
    constructor(el: ElementRef) {
        this.ref = el.nativeElement;
        this.modalCss();
    }

    @HostListener('click') click() {
        if (event.target === this.ref) {
            this.ref.style.display = 'none';
        }
    }

    private modalCss() {
        this.ref.style.display = 'none';
        this.ref.style.position = 'fixed';
        this.ref.style.top = 0;
        this.ref.style.bottom = 0;
        this.ref.style.left = 0;
        this.ref.style.right = 0;
        this.ref.style.backgroundColor = 'rgba(0, 0, 0, 0.8)';
        this.ref.style.zIndex = 2147483647;
    }
}
