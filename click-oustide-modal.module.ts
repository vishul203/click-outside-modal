import { NgModule } from '@angular/core';

import { ClickOutsideModalDirective } from './click-outside.directive';
import { triggerModalDirective } from './trigger-modal.directive';

@NgModule({
  declarations: [ClickOutsideModalDirective, triggerModalDirective],
  exports: [ClickOutsideModalDirective, triggerModalDirective]
})
export class ClickOutsideModalModule {}
